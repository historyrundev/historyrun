-----------------------------------------------------------------------------------------
--
-- img.lua
--
-----------------------------------------------------------------------------------------

local img = {

	-- menu
	mainMenuBackground   	=   "img/background.png",
	logo 					=	"img/logo.png",
	settingMain				=	"img/settings.png",
	backMenu				=	"img/back.png",
	skinMenu				= 	"img/skin.png",
	level1					=	"img/level1.png",
	level2					=	"img/level2.png",
	level3					=	"img/level3.png",
	checkOk					= 	"img/ok.png",
	text 					=	"img/textIntro.png",
	tastoPlay				=	"img/tastoPlay.png",
	schermo					=	"img/schermo.png",

	--common game
	corsaSprite 	= 	"img/corsa.png",
	cuore 			=	"img/cuore.png",
	pause           =   "img/pause.png",
	moneta          =   "img/moneta.png",
	gameOverSprite  =   "img/gameover.png",
	hackItAgain     =   "img/hackitagain.png",
	
	-- level 1
	cielo 			=	"img/cielo.png",
	piramidi 		=	"img/piramidi.png",
	dune 			= 	"img/dune.png",
	pavimento 		=	"img/floor.png",
	piattaforma     =   "img/piattaforma.png",
	freccia 		=	"img/freccia.png",
	lancia 			=	"img/lancia.png",
	mummie 			=	"img/mummie.png",
	spuntoni		=	"img/spuntoni.png"
}

return img