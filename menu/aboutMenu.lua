-----------------------------------------------------------------------------------------
--
-- aboutMenu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations and other locals
local playBtn

-- 'onRelease' event listener for backMenu
local function onBackMenuRelease()
	
	-- go to mainMenu.lua scene
	composer.gotoScene( "menu.mainMenu", "fade", 900 )
	
	return true	-- indicates successful touch
end


function scene:create( event )
	local sceneGroup = self.view

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	-- display a background image
	local background = display.newImageRect( img.mainMenuBackground, display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	
	-- create/position logo/title image on upper-half of the screen
	local titleLogo = display.newImageRect( img.logo, display.contentWidth*0.4, display.contentWidth*0.1 )
	titleLogo.x = display.contentCenterX
	titleLogo.y = display.contentHeight*0.12
	
	--local text = display.newText("HACKING TIME - Escapeing from the past\n\nCreated by:\n    - Sbano Giuseppe\n\t    - Sepielli Andrea\n\t    - Tallarico Luca\n\t    - Trotto Federico\n\t    - Zoccari Chiara\n\nApplication created for the ''Mobile Computing''\ncourse, presented by Professor Franco Milicchio,\nacademic year 2018-2019.\n\nDepartment of Engineering Roma Tre University\nVia della Vasca Navale, 79 - 00146 - Rome, Italy", display.contentCenterX, display.screenOriginY, native.systemFont, 18)
	local text = display.newImageRect( img.text, display.contentWidth*0.9, display.contentWidth*0.45 )
	text:setFillColor( 1, 1, 1 )
	text.x = display.contentCenterX*1
	text.y = display.contentHeight*0.65

	-- create a widget button back
	playBtnBackMenu = widget.newButton{
		defaultFile = img.backMenu,
		overFile    = img.backMenu,
		width       = display.contentWidth*0.1, height=display.contentWidth*0.06,
		onRelease   = onBackMenuRelease	-- event listener function
	}
	playBtnBackMenu.x = display.contentCenterX*0.12
	playBtnBackMenu.y = display.contentHeight*0.08

	
	-- all display objects must be inserted into group
	sceneGroup:insert( background )
	sceneGroup:insert( titleLogo )
	sceneGroup:insert( text )
	sceneGroup:insert( playBtnBackMenu )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.

	if playBtnSkinMenu then
		playBtnSkinMenu:removeSelf()		-- widgets must be manually removed
		playBtnSkinMenu = nil
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene