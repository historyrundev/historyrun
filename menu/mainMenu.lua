-----------------------------------------------------------------------------------------
--
-- mainMenu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"
local sfx = require "audio.sfx"

--------------------------------------------

-- forward declarations and other locals
local playBtn

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	-- go to levelSelect.lua scene
	composer.gotoScene( "menu.levelSelect", "fade", 900 )
	
	return true	-- indicates successful touch
end

local function onPlayBtnSettingMainRelease()
	
	-- go to levelSelect.lua scene
	composer.gotoScene( "menu.aboutMenu", "fade", 900 )
	
	return true	-- indicates successful touch
end

function scene:create( event )
	local sceneGroup = self.view

	local options =
{
    width = 1000,
    height = 600,
    numFrames = 15,
}

	local sequenceData =
	{
	name = "schermo",
	start = 1,
	count = 15,
	time = 3000,
	loopCount = 0,
	loopDirection = "forward"
}

	local sheet = graphics.newImageSheet(img.schermo, options)

    local background = display.newSprite( sheet, sequenceData )
	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	-- display a background image
	--background.anchorX = 0
	--background.anchorY = 0
	  background.x = display.contentCenterX
      background.y = 189
	  background:scale (0.64,0.64)


	background:play()

	
	-- create/position logo/title image on upper-half of the screen
	--local titleLogo = display.newImageRect( img.logo, display.contentWidth*0.8, display.contentWidth*0.2 )
	--titleLogo.x = display.contentCenterX
	--titleLogo.y = display.contentHeight*0.4
	
	-- create a widget button (which will loads level1.lua on release)
	playBtn = widget.newButton{
		defaultFile = img.tastoPlay,
		overFile    = img.tastoPlay,
		width       = display.contentWidth*0.2, height=display.contentWidth*0.08,
		onRelease   = onPlayBtnRelease	-- event listener function
	}	
	playBtn.x = display.contentCenterX
	playBtn.y = display.contentHeight*0.80-- -40

	-- create a widget button setting
	playBtnSettingMain = widget.newButton{
		defaultFile = img.settingMain,
		overFile    = img.settingMain,
		width       = display.contentWidth*0.1, height=display.contentWidth*0.06,
		onRelease   = onPlayBtnSettingMainRelease	-- event listener function
	}
	playBtnSettingMain.x = display.contentCenterX*0.12
	playBtnSettingMain.y = display.contentHeight*0.08
	
	-- all display objects must be inserted into group
	sceneGroup:insert( background )
	--sceneGroup:insert( titleLogo )
	sceneGroup:insert( playBtn )
	sceneGroup:insert( playBtnSettingMain )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
		if not audio.isChannelActive( 1 ) then
			sfx.playMusic(sfx.bgMenuMusic, {channel = 1 , loops=-1} )
			end
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
	elseif phase == "did" then
	
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	audio.stop()
	audio.dispose(sfx.bgMenuMusic)
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end

	if playBtnSettingMein then
		playBtnSettingMein:removeSelf()		-- widgets must be manually removed
		playBtnSettingMein = nil
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene