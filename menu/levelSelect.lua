-----------------------------------------------------------------------------------------
--
-- levelSelect.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"


--------------------------------------------

-- forward declarations and other locals
local playBtn

-- 'onRelease' event listener for playBtn
local function onPlayBtnLevel1Release()
	
	-- go to level1.lua scene
	composer.gotoScene( "levels.level1", "fade", 900 )
	
	return true	-- indicates successful touch
end
-- 'onRelease' event listener for playBtn
local function onPlayBtnLevel2Release()
	
	-- go to level1.lua scene
	composer.gotoScene( "core.game", "fade", 900 )
	
	return true	-- indicates successful touch
end
-- 'onRelease' event listener for playBtn
local function onPlayBtnLevel3Release()
	
	-- go to level1.lua scene
	composer.gotoScene( "core.game", "fade", 900 )
	
	return true	-- indicates successful touch
end

-- 'onRelease' event listener for backMenu
local function onBackMenuRelease()
	
	-- go to mainMenu.lua scene
	composer.gotoScene( "menu.mainMenu", "fade", 900 )
	
	return true	-- indicates successful touch
end

-- 'onRelease' event listener for backMenu
local function onSkinMenuRelease()
	
	-- go to skinMenu.lua scene
	composer.gotoScene( "menu.mainMenu", "fade", 900 )
	
	return true	-- indicates successful touch
end

function scene:create( event )
	local sceneGroup = self.view

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	-- display a background image
	local background = display.newImageRect( img.mainMenuBackground, display.actualContentWidth, display.actualContentHeight )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX 
	background.y = 0 + display.screenOriginY
	
	-- create/position logo/title image on upper-half of the screen
	local titleLogo = display.newImageRect( img.logo, display.contentWidth*0.4, display.contentWidth*0.1 )
	titleLogo.x = display.contentCenterX
	titleLogo.y = display.contentHeight*0.12
																												  --native.systemFont
	local titlelevel = display.newText("# Select a level:", display.contentCenterX*1.5, display.screenOriginY*0.8, "Courier New Bold.ttf", 24)
	titlelevel:setFillColor( 1, 1, 1 )
	titlelevel.x = display.contentCenterX*0.35
	titlelevel.y = display.contentHeight*0.3
    
	
	-- create a widget button level1.lua
	playBtnlevel1 = widget.newButton{
		defaultFile = img.level1,
		overFile    = img.level1,
		width       = display.contentWidth*0.25, height=display.contentWidth*0.15,
		onRelease   = onPlayBtnLevel1Release	-- event listener function
	}	
	playBtnlevel1.x = display.contentCenterX*0.4
	playBtnlevel1.y = display.contentHeight*0.5

	-- create a widget button level2.lua
	playBtnlevel2 = widget.newButton{
		defaultFile = img.level2,
		overFile    = img.level2,
		width       = display.contentWidth*0.25, height=display.contentWidth*0.15,
		onRelease   = onPlayBtnLevel2Release	-- event listener function
	}	
	playBtnlevel2.x = display.contentCenterX*1
	playBtnlevel2.y = display.contentHeight*0.5

	-- create a widget button level3.lua
	playBtnlevel3 = widget.newButton{
		defaultFile = img.level3,
		overFile    = img.level3,
		width       = display.contentWidth*0.25, height=display.contentWidth*0.15,
		onRelease   = onPlayBtnLevel3Release	-- event listener function
	}	
	playBtnlevel3.x = display.contentCenterX*1.6
	playBtnlevel3.y = display.contentHeight*0.5

	-- create a widget button back
	playBtnBackMenu = widget.newButton{
		defaultFile = img.backMenu,
		overFile    = img.backMenu,
		width       = display.contentWidth*0.1, height=display.contentWidth*0.06,
		onRelease   = onBackMenuRelease	-- event listener function
	}
	playBtnBackMenu.x = display.contentCenterX*0.12
	playBtnBackMenu.y = display.contentHeight*0.08

	-- create a widget button skins
	playBtnSkinMenu = widget.newButton{
		defaultFile = img.skinMenu,
		overFile    = img.skinMenu,
		width       = display.contentWidth*0.15, height=display.contentWidth*0.06,
		onRelease   = onSkinMenuRelease	-- event listener function
	}
	playBtnSkinMenu.x = display.contentCenterX*1.83
	playBtnSkinMenu.y = display.contentHeight*0.1
	
	-- all display objects must be inserted into group
	sceneGroup:insert( background )
	sceneGroup:insert( titleLogo )
	sceneGroup:insert( titlelevel )

	sceneGroup:insert( playBtnlevel1 )
	sceneGroup:insert( playBtnlevel2 )
	sceneGroup:insert( playBtnlevel3 )
	sceneGroup:insert( playBtnBackMenu )
	sceneGroup:insert( playBtnSkinMenu )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		audio.fadeOut( { channel = 1, time = 100 } )
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	
	if playBtnlevel1 then
		playBtnlevel1:removeSelf()	-- widgets must be manually removed
		playBtnlevel1 = nil
	end

	if playBtnlevel2 then
		playBtnlevel2:removeSelf()	-- widgets must be manually removed
		playBtnlevel2 = nil
	end

	if playBtnlevel3 then
		playBtnlevel3:removeSelf()	-- widgets must be manually removed
		playBtnlevel3 = nil
	end

	if playBtnSkinMenu then
		playBtnSkinMenu:removeSelf()		-- widgets must be manually removed
		playBtnSkinMenu = nil
	end

	if playBtnBackMenu then
		playBtnBackMenu:removeSelf()		-- widgets must be manually removed
		playBtnBackMenu = nil
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene