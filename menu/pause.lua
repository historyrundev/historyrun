local composer = require("composer")

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local function gotoGame()
	composer.hideOverlay("fade", 400)
	return true -- Prevents touch propagation
end

local function gotoMenu()
	composer.gotoScene("menu.mainMenu", {time = 800, effect = "crossFade"})
	composer.hideOverlay("fade", 400)
	return true -- Prevents touch propagation
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create(event)
	local sceneGroup = self.view -- Any display object which should be part of the scene must be inserted into the scene's view group
	-- Code here runs when the scene is first created but has not yet appeared on screen

	-- Declaring a rectangle to fill the screen and achieve a blurring effect of the game
	local rect =
		display.newRect(
		sceneGroup,
		display.contentCenterX,
		display.contentCenterY,
		display.actualContentWidth,
		display.actualContentHeight
	)
	rect:setFillColor(255 / 255, 255 /255, 255 / 255, 0.7) -- The 4th parameters is Alpha

	-- The score is contained in the composer variable currentScore, could change to be a parameter of the pause menu
	local menuStartingPosition = display.contentHeight * 0.25

	local score =
		display.newText(
		sceneGroup,
		"1",
		display.contentCenterX,
		menuStartingPosition,
		native.systemFont,
		28
	)

	local resumeButton =
		display.newText(
		sceneGroup,
		"Resume",
		display.contentCenterX,
		menuStartingPosition + display.contentHeight / 5,
		native.systemFont,
		28
	)

	local gotoMenuButton =
		display.newText(
		sceneGroup,
		"Back to the main menu",
		display.contentCenterX,
		menuStartingPosition + display.contentHeight / 3,
		native.systemFont,
		28
	)

	resumeButton:addEventListener("tap", gotoGame)
	gotoMenuButton:addEventListener("tap", gotoMenu)
end

-- show()
function scene:show(event)
	local sceneGroup = self.view
	local phase = event.phase

	if (phase == "will") then
		-- Code here runs when the scene is still off screen (but is about to come on screen)
	elseif (phase == "did") then
	-- Code here runs when the scene is entirely on screen
	end
end

-- hide()
function scene:hide(event)
	local sceneGroup = self.view
	local phase = event.phase
	local parent = event.parent -- Reference to the parent, the game scene

	if (phase == "will") then
		-- Code here runs when the scene is on screen (but is about to go off screen)

		parent:resumeGame() -- If the pause screen is about to be hidden, resume the game
	elseif (phase == "did") then
	-- Code here runs immediately after the scene goes entirely off screen
	end
end

-- destroy()
function scene:destroy(event)
	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view
end

-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)
-- -----------------------------------------------------------------------------------

return scene
