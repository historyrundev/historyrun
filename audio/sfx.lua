--
-- Sound effects management
--

local audio = require( "audio" )
local sfx = {
  bgMenuMusic  = audio.loadStream( "audio/mainMenu.mp3" ),
  bgLvlMusic   = audio.loadStream( "audio/level1music.mp3" ),
  gameOverSound = audio.loadStream( "audio/gameoversound.mp3" ),
  jumpEffect = audio.loadStream( "audio/jumpeffect.mp3" ),
  hitEffect = audio.loadStream( "audio/hiteffect.mp3" ),
  shootEffect = audio.loadStream( "audio/shooteffect.wav" ),
  slideEffect = audio.loadStream( "audio/slideeffect.mp3" )
}

-- inizialize channels and volumes
sfx.init = function()
	-- reserve 10 audio channels
   audio.reserveChannels(10)
   audio.setVolume( 100, { channel = 1 } )  --background music
   audio.setVolume( 100, { channel = 2 } )  --jump effect
   audio.setVolume( 100, { channel = 3 } )  --hit effect
   audio.setVolume( 100, { channel = 4 } )  --shoot Effect
   audio.setVolume( 100, { channel = 5 } )  --slide effect
   audio.setVolume( 100, { channel = 6 } )  --game over
   
   end
   
sfx.playSound = function( handle, options )
   
   audio.play( handle, options )
end

sfx.playMusic = function( handle, options )
   
   audio.rewind(handle)
   audio.play( handle, options )
end

sfx.pauseSound = function()
 	audio.setVolume( 0, { channel=2 } )
    audio.setVolume( 0, { channel=3 } )    
    audio.setVolume( 0, { channel=4 } )
    audio.setVolume( 0, { channel=5 } )
    audio.setVolume( 0, { channel=6 } )
    audio.setVolume( 0, { channel=7 } )
    audio.setVolume( 0, { channel=9 } )
    audio.setVolume( 0, { channel=10 } )
end

return sfx