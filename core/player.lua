-----------------------------------------------------------------------------------------
--
-- player.lua
--
-----------------------------------------------------------------------------------------

local character         =     require ("core.character")
local physics 					=			require( "physics" )
local sfx               =     require ( "audio.sfx" )
local composer          =     require ( "composer" )

local player = {}
player.__index = player

--creating the metatable that permits to use the table like an extension of
--the base class character
setmetatable(player, {
  __index = character, -- this is what makes the inheritance work
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})

--[[function player:_init(init1, init2)
  character._init(self, init1) -- call the base class constructor
  self.value2 = init2
end]]

-- This function serves to compensate for the displacement of the player, 
--even if minimal, by using a transition that always returns it to its place

function  player:stability(event)
    if(event.phase == "began") then
      local player = player:getCharacter()
      transition.to( player, { 1000, x=display.contentWidth/2})
  -- body
 end
end



--I place the player's lives 
--in the upper right corner

local lifeIcons = {}
local lives = 3
local maxLives = 3
local i
for i = 1, maxLives do
    lifeIcons[i] = display.newImage(img.cuore)
    lifeIcons[i].x = display.contentWidth/6 + (lifeIcons[i].contentWidth * (i - 1))
    lifeIcons[i].y = 30 -- start at 10,10
end
  
 --When the collision is activated, 
--the function starts and if you have more 
--than 0 hearts it makes one invisible, otherwise the game over starts 

function player:damege(event)
if(event.phase == "began") then
if lives > 1 then
    lifeIcons[lives].isVisible = false
    lives = lives - 1
    print("Hit!")
	sfx.playMusic(sfx.hitEffect, {channel = 3 , loops=0})
	else
	lives = 0
	lifeIcons[1].isVisible = false
	sfx.playMusic(sfx.gameOverSound, {channel = 6 , loops=0})
  end
  end
end


function player:setPhysics(Player,density,friction,bounce,shape,filter)
  -- body
     self.physics = physics.addBody( Player, {density = density, 
                                            friction= friction, 
                                            bounce = bounce, 
                                             shape = shape,
                                            filter = filter} )
end

function player:getPhysics()


  -- body
  return self.physics
end


function player:setMyName(myName)
	self.myName = "player"
end

function player:getMyName()
	
	return self.myName
end





-----------------------------------------------------------------------------------------
--player general settings
player:setAnimation(img.corsaSprite)									                --setting animation
player:setSeqOpt("fastRun",1,8,700,0,"forward")					      		    --setting sequenceData
player:setOptions(93,89,8)												                    --setting options
player:setShape(player:getOptions().width,player:getOptions().height)	--setting shape
player:setX(display.contentWidth/2)										                --setting coordinate X
player:setY(-50)													                            --setting coordinate Y
-----------------------------------------------------------------------------------------
--setting the character features
player:setCharacter(player:getAnimation(),			
					player:getOptions(),		
					player:getSeqOpt(),			
					player:getShape(),			
					player:getX(),				
					player:getY())				

player:setPhysics(player:getCharacter(), 1, 0.3, 0, characterShape,filter.playerFilter)

-----------------------------------------------------------------------------------------
return player