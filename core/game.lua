--------------------------------------------------------------------------------------
-- game.lua
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
-- This class is the manager of game loop for all levels
--------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--
-- game.lua
--
-----------------------------------------------------------------------------------------

--including libraries
local composer					=			require( "composer" )
local scene 					=			composer.newScene()
local moveSettings 				=			require( "core.moveSettings" )
local timer 					=			require( "timer" )
local playerActions 			= 			require( "core.playerActions" )
local physics 					=			require( "physics" )
local controls 					=			require( "core.controls" ) 
local Player 					=			require( "core.player" )
local Enemy						=			require( "core.enemy" )		
local arrow                     =           require( "core.arrow" )
local level						=			require( "levels.level2")

physics.start()
physics.setGravity(0,display.contentHeight/10)

--include actions the player can do and control buttons
controls:load()

--------------------------------------------
-- forward declarations and other locals
local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentCenterX

-- Set up a few local variables for convenience
local _w = display.contentWidth -- Width of screen
local _h = display.contentHeight  -- Height of screen
local _x = _w/2  -- Horizontal centre of screen
local _y = _h/2  -- Vertical centre of screen

--loading player
local player = Player:getCharacter()
--loading enemy
local enemy = Enemy:getCharacter()
--loading arrow
local arrow = arrow:getCharacter()	


--I start gravity for the arrow

arrow.gravityScale = 0
-- handle movement
-- This function invokes the function in the previously created library. 
-- It must be an "event" because only then can it be implemented in Runtime
local function movement(event)
	moveSettings:move(1, level.background0)
	moveSettings:move(0.5, level.background1)
	moveSettings:move(2, level.background2)
	moveSettings:move(4, level.pavement)
end


local pauseButton = display.newImageRect(img.pause, 50,50)
	pauseButton.x = display.contentWidth * 0.96
	pauseButton.y = display.contentHeight / 15
-- Function to handle the pausing of the game, by halting game functions and showing an overlay
local function pauseButtonTap(event)

local pauseOptions = {
	isModal = true,
	effect = "fade",
	time = 400
}
	pauseButton.xScale = 0.80 -- effect pressed button
	pauseButton.yScale = 0.80 -- effect pressed button
	physics.pause()
	player:pause()
	enemy:pause()
	Runtime:removeEventListener("enterFrame", movement)
	composer.showOverlay("menu.pause", pauseOptions)
	return true
end

function scene:resumeGame()
	pauseButton.xScale = 1 -- Resizing the pause button to make it "feel" pressed
	pauseButton.yScale = 1 -- Resizing the pause button to make it "feel" pressed
	physics.start()
	player:play()
	enemy:play()
	Runtime:addEventListener( "enterFrame", movement )
end

function scene:create( event )

	local sceneGroup = self.view

	physics.start()
	physics.pause()
	
	player:play()
	enemy:play()
    arrow:play()

	-- define a shape that's slightly shorter than image bounds (set draw mode to "hybrid" or "debug" to see)
	local floorShape = { -halfW,-display.contentHeight/10.5, halfW,-display.contentHeight/10.5, halfW,display.contentHeight/10, -halfW,display.contentHeight/10 }
	physics.addBody( level.pavement[0], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter } )
	physics.addBody( level.pavement[1], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter} )
	physics.addBody( level.pavement[2], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter } )

	-- all display objects must be inserted into group
	backgroundGroup = display.newGroup() --group for background
	backgroundGroup:insert( level.background0[0] )
	backgroundGroup:insert( level.background0[1] )
	backgroundGroup:insert( level.background0[2] )

	backgroundGroup:insert(level.background1[0])
	backgroundGroup:insert(level.background1[1])
	backgroundGroup:insert(level.background1[2])

	backgroundGroup:insert(level.background2[0])
	backgroundGroup:insert(level.background2[1])
	backgroundGroup:insert(level.background2[2])

	backgroundGroup:insert(level.pavement[0])
	backgroundGroup:insert(level.pavement[1])
	backgroundGroup:insert(level.pavement[2])

	sceneGroup:insert(backgroundGroup)
	sceneGroup:insert( player )
   

end

pauseButton:addEventListener("tap", pauseButtonTap)

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
		-- mettere i timer
		--audio.play(sfx.bgLvlMusic)
		sfx.playMusic(sfx.bgLvlMusic, {channel = 1 , loops=-1})
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		--mettere i timer qui
		
		physics.start()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		audio.fadeOut(1,10)
		physics.stop()
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	
	package.loaded[physics] = nil
	physics = nil
	audio.dispose()
end

 -----------------------------------
-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
Runtime:addEventListener( "enterFrame", movement )
--Runtime:addEventListener("enterFrame", enemyflying)

----------------------------------------------------------------------------------------

return scene
--[[
local composer = require("composer")
local physics = require("physics")
local moveSettings 				=			require( "core.moveSettings" )
local timer 					=			require( "timer" )
local playerActions 			= 			require( "core.playerActions" )
local controls 					=			require( "core.controls" ) 
local Player 					=			require( "core.player" )
local Enemy						=			require( "core.enemy" )		
local arrow                     =           require( "core.arrow")

--load selected level
local level = require("levels.level1")

local scene = composer.newScene()

physics.start()
physics.setGravity(0, 80)

--initialize variables
local lives = 3
local score = 0

--include actions the player can do and control buttons
controls:load()

--------------------------------------------
-- forward declarations and other locals
local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentCenterX

-- Set up a few local variables for convenience
local _w = display.contentWidth -- Width of screen
local _h = display.contentHeight  -- Height of screen
local _x = _w/2  -- Horizontal centre of screen
local _y = _h/2  -- Vertical centre of screen

--loading player
local player = Player:getCharacter()
--loading enemy
local enemy = Enemy:getCharacter()
--loading arrow
local arrow = arrow:getCharacter()	


--I start gravity for the arrow

arrow.gravityScale = 0

--gameover function
local function endGame()
	display.remove(player)
	local gameOverText =
		display.newText(uiGroup, "GAME OVER", display.contentWidth / 2, display.contentHeight / 2, native.systemFont, 32)
	composer.setVariable("finalScore", score)

	-- Moves the end game screen to the bottom of the display and then shows the high scores
	local transition = function()
		transition.to(
			gameOverText,
			{
				y = display.contentHeight,
				alpha = 0,
				onComplete = function()
					display.remove(gameOverText)
				end
			}
		)
		composer.gotoScene("scenes.highscores", {time = 800, effect = "crossFade"})
	end
end

-- Function to handle the pausing of the game, by halting game functions and showing an overlay
local function pauseButtonTap(event)


function scene:create( event )

	local sceneGroup = self.view

	physics.start()
	physics.pause()
	
	player:play()
	enemy:play()
    arrow:play()

	-- define a shape that's slightly shorter than image bounds (set draw mode to "hybrid" or "debug" to see)
	local floorShape = { -halfW,-display.contentHeight/10.5, halfW,-display.contentHeight/10.5, halfW,display.contentHeight/10, -halfW,display.contentHeight/10 }
	physics.addBody( floor[0], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter } )
	physics.addBody( floor[1], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter} )
	physics.addBody( floor[2], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter } )

	-- all display objects must be inserted into group
	sceneGroup:insert( sky[0] )
	sceneGroup:insert( sky[1] )
	sceneGroup:insert( sky[2] )

	sceneGroup:insert(pyramids[0])
	sceneGroup:insert(pyramids[1])
	sceneGroup:insert(pyramids[2])

	sceneGroup:insert(dune[0])
	sceneGroup:insert(dune[1])
	sceneGroup:insert(dune[2])

	sceneGroup:insert(floor[0])
	sceneGroup:insert(floor[1])
	sceneGroup:insert(floor[2])

	sceneGroup:insert( player )
   

end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
		-- mettere i timer
		--audio.play(sfx.bgLvlMusic)
		sfx.playMusic(sfx.bgLvlMusic, {channel = 1 , loops=-1})
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		--mettere i timer qui
		
		physics.start()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		audio.fadeOut(1,10)
		physics.stop()
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	
	package.loaded[physics] = nil
	physics = nil
	audio.dispose()
end


-- handle movement
-- This function invokes the function in the previously created library. 
-- It must be an "event" because only then can it be implemented in Runtime
local function movement(event)
	moveSettings:move(1, sky)
	moveSettings:move(0.5, pyramids)
	moveSettings:move(2, dune)
	moveSettings:move(4, floor)
end

 -----------------------------------
-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
Runtime:addEventListener( "enterFrame", movement )

return scene
]]--