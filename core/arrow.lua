---------------------------------------------
--
---Arrow
-------------------------------------------------------------

local character                 =           require ("core.character")
local physics                   =            require( "physics" )

local arrow = {}
arrow.__index = arrow

--creating the metatable that permits to use the table like an extension of
--the base class character
setmetatable(arrow, {
  __index = character, -- this is what makes the inheritance work
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})

arrow.gravityScale = 0


--This function deletes an object so as not to weigh down the code
 function arrow:clear( event)
  if(event.phase == "began") then
    local arrow = arrow:getCharacter()
    arrow.isVisible=false
    transition.to( arrow, { time=math.random(6000,7000), x=display.contentWidth-1100})

end
end

--  This function handles the arrow
-- when it does not collide but ends 
-- its transaction. Everything is managed by time

    function arrow:spawn()
  local arrow = arrow:getCharacter()
  arrow.x = display.contentWidth-1100
  arrow.y = 250
  arrow.isVisible = true
  transition.to( arrow, { time=math.random(6000,7000), x=display.contentWidth+1100})

end

timer.performWithDelay( 10000, arrow.spawn,0 )


------------------------------------------------------------------------------------------
--setter function for the enemy transition 
function arrow:setTransition(Arrow,Time,coordX,Delay)
  --the arrow moves from  off-side screen to the inside, 
  -- time explains how long this transition takes place and
  -- with "delay" imposed after how much time it should move 
  self.transition = transition.to (Arrow,{time = Time, 
                                              x=coordX, 
                                              delay= Delay} )
end
--getter function for the enemy transition 
function arrow:getTransition(self)
  return self.transition
end

--A kinematic body is very similar to a dynamic body, 
--only it does not have gravity and force

function arrow:setPhysics(Arrow,density,friction,bounce,shape,filter)
  -- body
     self.physics = physics.addBody( Arrow, "kinematic", {density = density, 
                                             friction= friction, 
                                             bounce = bounce, 
                                             shape = shape,
                                             filter = filter} )
end

function arrow:getPhsycs(self)


  -- body
  return self.physics 
end



-----------------------------------------------------------------------------------------
--arrow general settings
--I inserted the sprite of the player, 
--as soon as there will be the sprite of the arrow, 
--we will insert it
arrow:setAnimation(img.freccia)                                                 --setting animation
arrow:setSeqOpt("fastRun",1,1,1000,0,"forward")                                 --setting options
arrow:setOptions(42,10,5)                                                       --setting sequenceData
arrow:setShape(arrow:getOptions().width,arrow:getOptions().height)              --setting shape
arrow:setX(display.contentWidth-1100)                                           --setting coordinate X
arrow:setY(2*display.contentHeight/3)                                           --setting coordinate Y

-----------------------------------------------------------------------------------------
--setting the character features 
arrow:setCharacter(arrow:getAnimation(),      
          arrow:getOptions(),   
          arrow:getSeqOpt(),      
          arrow:getShape(),     
          arrow:getX(),       
          arrow:getY()) 
-----------------------------------------------------------------------------------------
--setting transition
arrow:setTransition(arrow:getCharacter(),5000,display.contentWidth+1100,3500)
arrow:setPhysics(arrow:getCharacter(), 1, 0.3, 0, characterShape,filter.arrowFilter)
-----------------------------------------------------------------------------------------


return arrow
