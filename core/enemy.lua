-----------------------------------------------------------------------------------------  
--
-- enemy.lua
--
-----------------------------------------------------------------------------------------

local character = require ("core.character")
local Player    = require ("core.player")

local enemy = {}
enemy.__index = enemy

 

--creating the metatable that permits to use the table like an extension of
--the base class character
setmetatable(enemy, {
  __index = character, -- this is what makes the inheritance work
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})






--------------------------ENEMY GENERAL---------------------------------------------------


-- in Enemy General the parameters of the follower are set, they can also be used by 
-- calling the function to use them on any enemy that has similar parameters. 
-- To implement these parameters, just create an "InitializateEnemy" function

------------------------------------------------------------------------------------------
--setter function for the enemy transition 
function enemy:setTransition(Enemy,Time,coordX,Delay)
  --the follower moves from  off-side screen to the inside, 
  -- time explains how long this transition takes place and
  -- with "delay" imposed after how much time it should move 
  self.transition = transition.to (Enemy, {time = Time, 
                                              x=coordX, 
                                              delay= Delay} )
end
--getter function for the enemy transition 
function enemy:getTransition(self)
  return self.transition
end

function enemy:setPhysics(Enemy,density,friction,bounce,shape,filter)
  -- body
     self.physics = physics.addBody( Enemy, {density = density, 
                                             friction= friction, 
                                             bounce = bounce, 
                                             shape = shape,
                                             filter = filter} )
end

function enemy:getPhsycs(self)


  -- body
  return self.physics
end



-----------------------------------------------------------------------------------------
--enemy general settings
enemy:setAnimation(img.mummie)									                              --setting animation
enemy:setSeqOpt("fastRun",1,10,1000,0,"forward")                              --setting options
enemy:setOptions(160,90,10)									                      		        --setting sequenceData
enemy:setShape(enemy:getOptions().width,enemy:getOptions().height)            --setting shape
enemy:setX(-display.contentWidth/4)								                	        --setting coordinate X
enemy:setY(-150)												                          		        --setting coordinate Y
-----------------------------------------------------------------------------------------
--setting the character features
enemy:setCharacter(enemy:getAnimation(),			
					enemy:getOptions(),		
					enemy:getSeqOpt(),			
					enemy:getShape(),			
					enemy:getX(),				
					enemy:getY())	
-----------------------------------------------------------------------------------------
--setting transition
enemy:setTransition(enemy:getCharacter(),1000,display.contentWidth/6,3500)
enemy:setPhysics(enemy:getCharacter(), 1, 0.3, 0, characterShape,filter.followerFilter)
-----------------------------------------------------------------------------------------
return enemy