---------------------------------------------
--
---coin
-------------------------------------------------------------
---------------------------------------------------------------------------------------------
---Premise                                                                                 --
-- This library works exactly with the arrow library,                                      --
--using the same functions as they have the same behavior.                                 --
--The only thing that changes is that the player can jump                                  --
--without problems on the coin, if needed it can be changed easily.                        --
--I can easily modify the spawn times, the height and the point where to make them appear  --
---------------------------------------------------------------------------------------------

local character                 =           require ("core.character")
local physics                   =            require( "physics" )

local coin = {}
coin.__index = coin

--creating the metatable that permits to use the table like an extension of
--the base class character
setmetatable(coin, {
  __index = character, -- this is what makes the inheritance work
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})



--This function deletes an object so as not to weigh down the code
 function coin:clear( event)
  if(event.phase == "began") then
    local coin = coin:getCharacter()
    coin.isVisible=false
    transition.to( coin, { time=math.random(6000,7000), x=display.contentWidth+1100})

end
end

  function coin:spawn()
  local coin = coin:getCharacter()
  coin.x = display.contentWidth+1100
  coin.y = 300
  coin.isVisible = true
  transition.to( coin, { time=math.random(6000,7000), x=display.contentWidth-1100})

end

timer.performWithDelay( 10000, coin.spawn,0 )


------------------------------------------------------------------------------------------
--setter function for the enemy transition 
function coin:setTransition(coin,Time,coordX,Delay)
  --the coin moves from  off-side screen to the inside, 
  -- time explains how long this transition takes place and
  -- with "delay" imposed after how much time it should move 
  self.transition = transition.to (coin,{time = Time, 
                                              x=coordX, 
                                              delay= Delay} )
end
--getter function for the enemy transition 
function coin:getTransition(self)
  return self.transition
end

function coin:setPhysics(coin,density,friction,bounce,shape,filter)
  -- body
     self.physics = physics.addBody( coin,"kinematic", {density = density, 
                                             friction= friction, 
                                             bounce = bounce, 
                                             shape = shape,
                                             filter = filter} )
end

function coin:getPhsycs(self)


  -- body
  return self.physics 
end



-----------------------------------------------------------------------------------------
--coin general settings
--I inserted the sprite of the arrow, 
--as soon as there will be the sprite of the coin, 
--we will insert it
coin:setAnimation(img.moneta)                                                --setting animation
coin:setSeqOpt("fastRun",1,6,1000,0,"forward")                              --setting options
coin:setOptions(52,50,6)                                                 --setting sequenceData
coin:setShape(coin:getOptions().width,coin:getOptions().height)            --setting shape
coin:setX(display.contentWidth+10)                                         --setting coordinate X
coin:setY(300)                                                              --setting coordinate Y
-----------------------------------------------------------------------------------------
--setting the character features 
coin:setCharacter(coin:getAnimation(),      
          coin:getOptions(),   
          coin:getSeqOpt(),      
          coin:getShape(),     
          coin:getX(),       
          coin:getY()) 
-----------------------------------------------------------------------------------------
--setting transition
coin:setTransition(coin:getCharacter(),10000,display.contentWidth-1100,3500)
coin:setPhysics(coin:getCharacter(), 1, 0.3, 0, characterShape,filter.arrowFilter)
-----------------------------------------------------------------------------------------


return coin
