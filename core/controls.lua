-----------------------------------------------------------------------------------------
--
-- controls.lua
--
-----------------------------------------------------------------------------------------
--controls.lua creates the control buttons(move and gun) and associates them the functions 
--using the eventListeners

--requiring necessary libraries
local playerActions            =         require( "core.playerActions" )
local Player                   =         require( "core.player" )
local enemy                    =         require( "core.enemy")
local Arrow                    =         require( "core.arrow")
local Coin                     =         require( "core.coin")
local score                    =         require( "lib.score")
-----------------------------------------------------------------------------------------
--creating the metatable that permits to use the table like a class
local controls = {}

controls.__index = controls

setmetatable(controls, {
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})
-----------------------------------------------------------------------------------------
--this function creates the buttons move and gun and memorizes them as attributes 
--of the controls class
function controls:createCmd()
	
	--move command
	local moveCmd = display.newRect(display.contentWidth/4,
									display.contentHeight/2,
									display.contentWidth/2,
									display.contentHeight)
	--move:setFillColor(0,0,0,2)
	moveCmd.alpha = 0.01
	moveCmd.id = "Move"
	
	--shoot command
	local shoot = display.newRect(display.contentWidth*0.75,
								  display.contentHeight/2,
								  display.contentWidth/2,
								  display.contentHeight)
	--shoot:setFillColor(1,1,1,1)
	shoot.alpha = 0.01
	shoot.id = "Shoot"

	self.move = moveCmd
	self.gun  = shoot
end
-----------------------------------------------------------------------------------------
--getter function of the move button
function controls:getMove()
	return self.move
end
--getter function of the gun button
function controls:getGun()
	return self.gun
end
-----------------------------------------------------------------------------------------
--function of collision detection between player and another object
local function onCollision( event )
	if(event.object1 == Player:getCharacter()) then
		playerActions:collides(event)
	end
end

-- function of collision detection between arrow and another object in filter library
local function Collision( event )
	if(event.object2 == Arrow:getCharacter()) then
		playerActions:setCanJump(false)
		Player:damege(event)
		Arrow:clear(event)
		Player:stability(event)
	end
end

-- -- function of collision detection between coin and another object in filter library
local function CollisionCoin(event)
    if(event.object2 == Coin:getCharacter()) then
     Coin:clear(event)
     score:add()
     Player:stability(event)

 end
end

--jump function called when the screen is tapped
--it updates directly the boolean canJump
local function jump( event )
	playerActions:jump(Player:getCharacter())
end
-----------------------------------------------------------------------------------------
-- slide function called when the finger moves on the screen 
-- only vertically and only from top to down
local function slide( event )
	playerActions:slide(event)
end

--shooting function(not ended)
local function gun( event )
	playerActions:shoot(event)
end
-----------------------------------------------------------------------------------------
--function called in the levels, it loads all the controls and their evenListeners
function controls:load()
	controls:createCmd()
    Runtime:addEventListener( "collision", onCollision )
    Runtime:addEventListener( "collision",Collision)
    Runtime:addEventListener( "collision",CollisionCoin)
	controls:getMove():addEventListener( "tap", jump )
	controls:getMove():addEventListener( "touch", slide )
	controls:getGun():addEventListener( "tap", gun )
end

-----------------------------------------------------------------------------------------
return controls