-----------------------------------------------------------------------------------------
--
-- moveSettings.lua
--
-----------------------------------------------------------------------------------------
--creating the metatable that permits to use the table like a class
local moveSettings = {}
moveSettings.__index = moveSettings

setmetatable(moveSettings, {
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})
-----------------------------------------------------------------------------------------
--the moveSetting.move function manages the movement of the various images, 
--to simplify the code we have put that each object starts from 0 and reaches 2, 
--so as to make the movement valid for each object in the game
function moveSettings:move(speed,object)
	local _w = display.contentWidth
	local screenW = display.contentWidth

    object[0].x = object[0].x-speed
    object[1].x = object[1].x-speed
    object[2].x = object[2].x-speed

    if(object[0].x < -screenW)then
        object[0].x = _w+object[2].contentWidth-speed
    elseif(object[1].x < -screenW)then
        object[1].x = _w+object[0].contentWidth-speed
    elseif (object[2].x < -screenW)then
    	object[2].x = _w+object[1].contentWidth-speed
    end
end
-----------------------------------------------------------------------------------------
--test of the move function
function moveSettings:moveTest(speed)
	print ("La speed è:"..speed)
end
-----------------------------------------------------------------------------------------
return moveSettings