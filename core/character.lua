-----------------------------------------------------------------------------------------
--
-- character.lua
--
-----------------------------------------------------------------------------------------

--character.lua creates a generical character(only the player now) and assign him all the 
--physical features and properties

local physics                  =         require( "physics" )
physics.start()

--creating the metatable that permits to use the table like a class
local character = {}
character.__index = character

setmetatable(character, {
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})
-----------------------------------------------------------------------------------------
--setter function for the character's animation 
function character:setAnimation(anim)
	self.animation = anim
end
--getter function for the character's animation
function character:getAnimation()  
	return self.animation
end
-----------------------------------------------------------------------------------------
--setter function for the character options(like width and height)
function character:setOptions(optW,optH,optNumF)
	self.options = 
	{
		width = optW,
		height = optH,
		numFrames = optNumF,
	}
end
--setter function for the character options
function character:getOptions()
	return self.options 
end
-----------------------------------------------------------------------------------------
--setter function for the character's sequenceData
function character:setSeqOpt(N,S,C,T,lC,lD)
	self.sequenceData = 
	{		
		name = N,
		start = S,
		count = C,
		time= T,
		loopCount = lC,
		loopDirection = lD
	}
end
--getter function for the character's sequenceData
function character:getSeqOpt()
	return self.sequenceData
end
-----------------------------------------------------------------------------------------
--getter function for the character's shape
function character:setShape(width,height)
	self.shape = 
	{
		-width/2,
		-height/2,
		width/2,
		-height/2,
		width/2,
		height/2,
		-width/2,
		height/2,
	}
end
--getter function for the character's shape
function character:getShape()
	return self.shape
end
-----------------------------------------------------------------------------------------
--setter function for the character's coordinate X
function character:setX(X)
	self.x = X
end
--getter function for the character's coordinate X
function character:getX()
	return self.x
end
-----------------------------------------------------------------------------------------
--setter function for the character's coordinate Y
function character:setY(Y)
	self.y = Y
end
--getter function for the character's coordinate Y
function character:getY()
	return self.y
end
-----------------------------------------------------------------------------------------
--this function is the setter of the character, here are defined all his features and is created 
--and memorized the character(only the player now) as an attribute of the character class
function character:setCharacter(animazione,opt,seq,Shape,charX,charY,Transition,myName)
	-- forward declarations and other locals
-----------------------------------------------------------------------------------------
--this is commented beacause is optional(useless for now)
	--local screenW, screenH, halfW = ScreenW, ScreenH, HalfW 
	--local _w = ScreenW -- Width of screen
	--local _h = ScreenH  -- Height of screen
	--local _x = _w/2  -- Horizontal centre of screen
	--local _y = _h/2  -- Vertical centre of screen
	-- Set up a few local variables for convenience
-----------------------------------------------------------------------------------------
	local options = opt

	local sheet = graphics.newImageSheet(animazione, options)
    

	local sequenceData = seq

	local character = display.newSprite( sheet, sequenceData )
----------------------------------------------------------------------------------------- 
--this is commented beacause is optional(useless for now) 
   	--[[local playerShape = { 
   						  -player.width/2,
						  -player.height/2, 
						   player.width/2,
						  -player.height/2,
						   player.width/2,
						   player.height/2,
						  -player.width/2,
						   player.height/2 
						}]]
-----------------------------------------------------------------------------------------	
	local characterShape = Shape

	-- physics.addBody( character, "dynamic", {density = 1.0, 
	-- 										friction=0.3, 
	-- 										bounce = 0 , 
	-- 										shape = characterShape,
	-- 										filter = filter.ArrowFilter} )
	--player:scale(0.6, 0.6)

	character.x, character.y = charX, charY 

	self.character = character
end
-----------------------------------------------------------------------------------------
--this function is the getter of the player, it returns the player object
function character:getCharacter()
	return self.character
end
-----------------------------------------------------------------------------------------
return character