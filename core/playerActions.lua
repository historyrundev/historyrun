-----------------------------------------------------------------------------------------
--
-- playerActions:lua
--
-----------------------------------------------------------------------------------------

--the playerActions:lua function manages all the actions the player has to do 
--during the game

--requiring necessary libraries
local timer                    =         require( "timer" )
local sfx = require ( "audio.sfx" )

--creating the metatable that permits to use the table like a class
local playerActions = {}
playerActions.__index = playerActions

setmetatable(playerActions, {
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})
-----------------------------------------------------------------------------------------
--canJump is a boolean variable of collision detection, if the player is in touch
--with the ground, canJump is true

function playerActions:setCanJump(boolean)
	self.canJump = boolean
end

function playerActions:getCanJump() 
	return self.canJump 
end
----------------------------------------------------
playerActions:setCanJump(true)
--boolean variable of slide down state, if the player is sliding the
--the value of isSliding is true
--local isSliding = false
----------------------------------------------------
--this function controls every frame if the player is in touch with the ground
function playerActions:collides(event)
	if ( event.phase == "began" ) then
        playerActions:setCanJump(true)
        print("began")
    end
end
----------------------------------------------------
--this function makes the player jump if the value of canJump is true
function playerActions:jump(object)
	if playerActions:getCanJump() == true then
		playerActions:setCanJump(false)
		sfx.playMusic(sfx.jumpEffect, {channel = 2 , loops=0})
		object:applyLinearImpulse(0,-display.contentHeight/2,object.x,object.y)
		print("Jump")
	end
end
----------------------------------------------------
--setter function of isSliding
function playerActions:setIsSliding(boolean)
    self.isSliding = boolean
end
--getter function of isSliding
function playerActions:getIsSliding()
	return self.isSliding
end
----------------------------------------------------
--this function updates value of isSliding to false, it is called from the timer
--every amount of time(1 second)
function endSliding()
	playerActions:setIsSliding(false)
end
----------------------------------------------------
playerActions:setIsSliding(false)
--this function makes the player slide on floor, it reaches the touch of the finger
--on the screen and permits the sliding action only if the touch respects a range 
--of coordinates on the screen and only if the movement of the finger is from top to down
function playerActions:slide(eventSlide)
	local swipeLengthY = math.abs(eventSlide.y - eventSlide.yStart) 
	local swipeLengthX = math.abs(eventSlide.x - eventSlide.xStart)
	if (eventSlide.phase == "moved") then
		if (swipeLengthX == 0) then
			if (eventSlide.yStart < eventSlide.y and swipeLengthY > 100) then
				if( playerActions:getIsSliding() == false and playerActions:getCanJump()==true) then
					playerActions:setIsSliding(true)
					sfx.playMusic(sfx.slideEffect, {channel = 5 , loops=0})
					print("Swiped Down")
					timer.performWithDelay(1000,endSliding)
				end
			end
		end
	end
end
----------------------------------------------------

--this function makes the player shoot the enemies 
--(the action algorithm mus be implemented yet)
function  playerActions:shoot(eventGun)
	print( "You touched the gun!" )
	sfx.playMusic(sfx.shootEffect, {channel = 4 , loops=0})
end
----------------------------------------------------

return playerActions
