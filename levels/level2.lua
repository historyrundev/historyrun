local level2 = {}

local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentCenterX

-- Set up a few local variables for convenience
local _w = display.contentWidth -- Width of screen
local _h = display.contentHeight  -- Height of screen
local _x = _w/2  -- Horizontal centre of screen
local _y = _h/2  -- Vertical centre of screen

--sky image
local sky={}
    sky[0] = display.newImageRect(img.cielo,_w,_h)
    sky[0].x = 0
    sky[0].y = _y
    sky[1] = display.newImageRect(img.cielo,_w,_h)
    sky[1].x = _w
    sky[1].y = _y
	sky[2] = display.newImageRect(img.cielo,_w,_h)
    sky[2].x = _w*2
    sky[2].y = _y

--pyramids image
local pyramids={}
    pyramids[0] = display.newImageRect(img.piramidi,_w,display.actualContentHeight/2.8)
    pyramids[0].x = 0
    pyramids[0].y = display.actualContentHeight/1.9
    pyramids[1] = display.newImageRect(img.piramidi,_w,display.actualContentHeight/2.8)
    pyramids[1].x = _w
    pyramids[1].y = display.actualContentHeight/1.9
	pyramids[2] = display.newImageRect(img.piramidi,_w,display.actualContentHeight/2.8)
    pyramids[2].x = _w+pyramids[1].contentWidth
    pyramids[2].y = display.actualContentHeight/1.9    
--dune image
local dune={}
    dune[0] = display.newImageRect( img.dune, display.contentWidth, display.actualContentHeight/4 )
	dune[0].x = 0
	dune[0].y = display.contentHeight*0.75
    dune[1] = display.newImageRect( img.dune, display.contentWidth, display.actualContentHeight/4 )
	dune[1].x = _w
	dune[1].y = display.contentHeight*0.75
	dune[2] = display.newImageRect( img.dune, display.contentWidth, display.actualContentHeight/4 )
	dune[2].x = _w+dune[1].contentWidth
	dune[2].y = display.contentHeight*0.75
--floor image
local floor={}
    floor[0] = display.newImageRect( img.pavimento, display.contentWidth, display.contentHeight/5 )
	floor[0].anchorY = 1
	--  draw the floor at the very bottom of the screen
	floor[0].x, floor[0].y = 0, display.actualContentHeight + display.screenOriginY + 20
    

    floor[1] = display.newImageRect( img.pavimento, display.contentWidth, display.contentHeight/5 )
	floor[1].anchorY = 1
	--  draw the floor at the very bottom of the screen
	floor[1].x, floor[1].y = _w, display.actualContentHeight + display.screenOriginY + 20
	

	floor[2] = display.newImageRect( img.pavimento, display.contentWidth, display.contentHeight/5 )
	floor[2].anchorY = 1
	--  draw the floor at the very bottom of the screen
	floor[2].x, floor[2].y = _w + floor[0].contentWidth, display.actualContentHeight + display.screenOriginY + 20 
	
level2.background0 = sky
level2.background1 = pyramids
level2.background2 = dune
level2.pavement = floor
return level2