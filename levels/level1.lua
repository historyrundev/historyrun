-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

--including libraries
local composer					=			require( "composer" )
local scene 					=			composer.newScene()
local moveSettings 				=			require( "core.moveSettings" )
local timer 					=			require( "timer" )
local playerActions 			= 			require( "core.playerActions" )
local physics 					=			require( "physics" )
local controls 					=			require( "core.controls" ) 
local Player 					=			require( "core.player" )
local Enemy						=			require( "core.enemy" )		
local arrow                     =           require( "core.arrow")
local score                     =           require( "lib.score")
local coin                      =           require( "core.coin")
physics.start()
physics.setGravity(0,display.contentHeight/10)

--include actions the player can do and control buttons
controls:load()

--------------------------------------------
-- forward declarations and other locals
local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentCenterX

-- Set up a few local variables for convenience
local _w = display.contentWidth -- Width of screen
local _h = display.contentHeight  -- Height of screen
local _x = _w/2  -- Horizontal centre of screen
local _y = _h/2  -- Vertical centre of screen

--loading player
local player = Player:getCharacter()
--loading enemy
local enemy = Enemy:getCharacter()
--loading arrow
local arrow = arrow:getCharacter()	
-- loandig coin
local coin = coin:getCharacter()


--I start gravity for the arrow

arrow.gravityScale = 0
-- the player cannot rotate
player.isFixedRotation = true 
----I start gravity for the arrow

 

 -- scoretext Lv1
local scoreText = score.init(
{
    fontSize = 20,
    font = "CoolCustomFont.ttf",
    x = display.contentCenterX,
    y = 30,
    maxDigits =3 ,
    leadingZeros = true
})


--sky image
sky={}
    sky[0] = display.newImageRect(img.cielo,_w,_h)
    sky[0].x = 0
    sky[0].y = _y
    sky[1] = display.newImageRect(img.cielo,_w,_h)
    sky[1].x = _w
    sky[1].y = _y
	sky[2] = display.newImageRect(img.cielo,_w,_h)
    sky[2].x = _w*2
    sky[2].y = _y

--pyramids image
pyramids={}
    pyramids[0] = display.newImageRect(img.piramidi,_w,display.actualContentHeight/2.8)
    pyramids[0].x = 0
    pyramids[0].y = display.actualContentHeight/1.9
    pyramids[1] = display.newImageRect(img.piramidi,_w,display.actualContentHeight/2.8)
    pyramids[1].x = _w
    pyramids[1].y = display.actualContentHeight/1.9
	pyramids[2] = display.newImageRect(img.piramidi,_w,display.actualContentHeight/2.8)
    pyramids[2].x = _w+pyramids[1].contentWidth
    pyramids[2].y = display.actualContentHeight/1.9    
--dune image
dune={}
    dune[0] = display.newImageRect( img.dune, display.contentWidth, display.actualContentHeight/4 )
	dune[0].x = 0
	dune[0].y = display.contentHeight*0.75
    dune[1] = display.newImageRect( img.dune, display.contentWidth, display.actualContentHeight/4 )
	dune[1].x = _w
	dune[1].y = display.contentHeight*0.75
	dune[2] = display.newImageRect( img.dune, display.contentWidth, display.actualContentHeight/4 )
	dune[2].x = _w+dune[1].contentWidth
	dune[2].y = display.contentHeight*0.75
--floor image
floor={}
    floor[0] = display.newImageRect( img.pavimento, display.contentWidth, display.contentHeight/5 )
	floor[0].anchorY = 1
	--  draw the floor at the very bottom of the screen
	floor[0].x, floor[0].y = 0, display.actualContentHeight + display.screenOriginY + 20
    

    floor[1] = display.newImageRect( img.pavimento, display.contentWidth, display.contentHeight/5 )
	floor[1].anchorY = 1
	--  draw the floor at the very bottom of the screen
	floor[1].x, floor[1].y = _w, display.actualContentHeight + display.screenOriginY + 20
	

	floor[2] = display.newImageRect( img.pavimento, display.contentWidth, display.contentHeight/5 )
	floor[2].anchorY = 1
	--  draw the floor at the very bottom of the screen
	floor[2].x, floor[2].y = _w + floor[0].contentWidth, display.actualContentHeight + display.screenOriginY + 20 

function scene:create( event )

	local sceneGroup = self.view

	physics.start()
	physics.pause()
	
	player:play()
	enemy:play()
    arrow:play()
    coin:play()
	-- define a shape that's slightly shorter than image bounds (set draw mode to "hybrid" or "debug" to see)
	local floorShape = { -halfW,-display.contentHeight/10.5, halfW,-display.contentHeight/10.5, halfW,display.contentHeight/10, -halfW,display.contentHeight/10 }
	physics.addBody( floor[0], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter } )
	physics.addBody( floor[1], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter} )
	physics.addBody( floor[2], "static", { friction=0.3, shape=floorShape, filter = filter.floorFilter } )

	-- all display objects must be inserted into group
	sceneGroup:insert( sky[0] )
	sceneGroup:insert( sky[1] )
	sceneGroup:insert( sky[2] )

	sceneGroup:insert(pyramids[0])
	sceneGroup:insert(pyramids[1])
	sceneGroup:insert(pyramids[2])

	sceneGroup:insert(dune[0])
	sceneGroup:insert(dune[1])
	sceneGroup:insert(dune[2])

	sceneGroup:insert(floor[0])
	sceneGroup:insert(floor[1])
	sceneGroup:insert(floor[2])

	sceneGroup:insert( player )
   

end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
		-- mettere i timer
		--audio.play(sfx.bgLvlMusic)
		sfx.playMusic(sfx.bgLvlMusic, {channel = 1 , loops=-1})
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		--mettere i timer qui
		
		physics.start()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		audio.fadeOut(1,10)
		physics.stop()
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	
	package.loaded[physics] = nil
	physics = nil
	audio.dispose()
end


-- handle movement
-- This function invokes the function in the previously created library. 
-- It must be an "event" because only then can it be implemented in Runtime
local function movement(event)
	moveSettings:move(1, sky)
	moveSettings:move(0.5, pyramids)
	moveSettings:move(2, dune)
	moveSettings:move(4, floor)
end

 -----------------------------------
-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
Runtime:addEventListener( "enterFrame", movement )
--Runtime:addEventListener("enterFrame", enemyflying)

----------------------------------------------------------------------------------------

return scene