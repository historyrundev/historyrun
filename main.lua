-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )
-- include the Corona "composer" module
composer = require ("composer")
img = require ("img.img")
filter = require ("lib.filter")
sfx = require ("audio.sfx")

-- Enable the multitouch 
system.activate( "multitouch" )

-- load menu screen
composer.gotoScene( "menu.mainMenu")