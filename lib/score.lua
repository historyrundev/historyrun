----------------------------------------------------------------------------------
--
--score.lua
--
--
---------------------------------------------------------------------------------------
--Premise                                                                            --
--this is a library already created by lua,                                          --
--a generic library taken from the libraries set by default                          --
-- by the programming language. Only changes have been made                          --
--on some variables and the score adder has been correctly increased.                --
--The Get and Set functions will then be useful to view the score at                 --
--the end of the level / end of the game or to reset everything.                     --
----------------------------------------------------------------------------------------
local score = {}
 
score.score = 0  -- Set the score to 0 initially
 
function score:init( options )
 
    local customOptions = options or {}
    local opt = {}
    opt.fontSize = customOptions.fontSize or 24
    opt.font = customOptions.font or native.systemFont
    opt.x = customOptions.x or display.contentCenterX
    opt.y = customOptions.y or opt.fontSize*0.5
    opt.maxDigits = customOptions.maxDigits or 6
    opt.leadingZeros = customOptions.leadingZeros or false
 
    local prefix = ""
    if ( opt.leadingZeros ) then
        prefix = "0"
    end
    score.format = "%" .. prefix .. opt.maxDigits .. "d"
 
    -- Create the score display object
    score.scoreText = display.newText( string.format( score.format, 0 ), opt.x, opt.y, opt.font, opt.fontSize )
 
    return score.scoreText
end
 

function score:set( value )
 
    score.score = tonumber(value)
    score.scoreText.text = string.format( score.format, score.score )
end
 
function score:get()
 
    return score.score
end
 
function score:add( amount )
 
    score.score = score.score + 0.5
    score.scoreText.text = string.format( score.format, score.score )
end
 
return score
