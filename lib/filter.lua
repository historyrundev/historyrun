-----------------------------------------------------------------------------------------
--
-- Filter.lua
--
-----------------------------------------------------------------------------------------
--This table with two values ​​manages collisions with the various game objects
----------------------------------------------------------------------------------------
local filter={

          arrowFilter    = { categoryBits = 4, maskBits = 9  },
          playerFilter   = { categoryBits = 1, maskBits = 14 },
          followerFilter = { categoryBits = 2, maskBits = 9  },
          floorFilter    = { categoryBits= 8, maskbits = 15  },

      }

return filter