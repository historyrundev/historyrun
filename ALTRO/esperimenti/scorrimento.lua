--/////////////---
---IMPORTANTE---
---///////////----

-- CI SONO 3  TIPI DI CODICE ENTRAMBI FUNZIONANTI 
-- DAL PUNTO DI VISTA DELLO SCORRIMENTO MA DA METTERE
-- ANCORA LE GIUSTE PROPORZIONI PER IL LOOP,SCEGLIEREMO
-- POI QUELLO CHE SI INTEGRA MEGLIO NEL CODICE FINALE.

--Dopo che avrà letto Peppone traduco i commenti --

--Non ho tenuto conto di mettere per verticale il telefono o dimensionare la foto
-- Ho semplicemente visto se funziona lo scorrimento e il loop


--Primo Codice

-- Ho integrato lo sfondo delle nuvole, i +100 e i -400
-- sono solo perchè volevo sistemarlo al volo in alto
--Primo Sfondo
local background = display.newImage("cielo1.png")
background.x = display.contentCenterX +100
background.y = display.contentCenterY -400
--Ho Messo più sfondi con la stessa immagine per rendere più semplice il loop
local background2 = display.newImage("cielo1.png")
background2.x = display.contentCenterX +100
background2.y = display.contentCenterY -400

local background3 = display.newImage("cielo1.png")
background3.x = display.contentCenterX +100
background3.y = display.contentCenterY -400

--Introduco la funzione per far scorrere il testo

local function move (event)
	 background.x =  background.x + scrollSpeed
	 background2.x = background2.x + scrollSpeed
	 background3.x = background3.x + scrollSpeed
     
    --loop dello sfondo, teoricamente dovrebbe andare
    --ma ho  i numeri messi sono a caso, non ho controllato la foto
    -- quindi si vede chiaramente il loop

    if(background.x + background.contentHeight) > 2040 then
    	background:translate(-960, 0)
    end 

     if(background2.x + background2.contentHeight) > 2040 then
    	background2:translate(-960, 0)
    end 
     if(background3.x + background3.contentHeight) > 2040 then
    	background3:translate(-960, 0)
    end 
end 

Runtime:addEventListener("enterFrame", move)



--Secondo tipo di codice--

local bg1 = display.newImage("cielo1.png", xWidth, yHeight)
bg1.x = display.contentCenterX - display.contentWidth
bg1.y = display.contentCenterY

local bg2 = display.newImage("cielo1.png", xWidth, yHeight)
bg2.x = display.contentCenterX - display.contentWidth
bg2.y = display.contentCenterY 

scroll = 8

--Scorrimento funzionante, loop dell'immagine ancora da mettere--

local function Scroll (event)
   bg1.x = bg1.x + scroll
    bg2.x = bg2.x + scroll
 
    if bg1.x == display.contentWidth * 1.5 then
        bg1.x = display.contentWidth * -.5
    end
 
    if bg2.x == display.contentWidth * 1.5 then
       bg2.x = display.contentWidth * -.5
    end
end
 
Runtime:addEventListener("enterFrame", Scroll)


--Terzo e ultimo codice--



local background = display.newImage("cielo1.png")
background.x = display.contentCenterX +100
background.y = display.contentCenterY -400

--Scorrimento funzionante, per il loop bisogna 
-- prendere bene i tempi , stesso problema precedente
-- si vede il loop quando avviene, bisogna calcolare meglio i tempi


function scroll(self,event)
	if  self.x < -180 then 
	self.x = 120
else
	self.x = self.x - 3
   end
end 
background.enterFrame = scroll
Runtime:addEventListener("enterFrame", background)